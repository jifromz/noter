/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import SwitchContainer from './app/components/RouteConfig';
import configureStore from './app/Store/index';
import {Provider} from 'react-redux';

import NavigationService from './NavigationService';
 
const store = configureStore();

class App extends Component { 
  render() {
    return (
      <Provider store={store}>
        <SwitchContainer
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default App;
