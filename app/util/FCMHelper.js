import firebase from 'react-native-firebase';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

storeData = async fcmToken => {
  try {
    await AsyncStorage.setItem('@fcmToken', fcmToken);
  } catch (e) {
    showAlert('Failed', 'Error on saving token.');
  }
};

export const getFcmToken = async () => {
  const fcmToken = await firebase.messaging().getToken();
  if (fcmToken) {    

    console.log(fcmToken);

    storeData(fcmToken);
    //  this.showAlert('Your Firebase Token is:', fcmToken);
  } else {
    showAlert('Failed', 'No token received.');
  }
};

requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission();
    // User has authorised
  } catch (error) {
    // User has rejected permissions
  }
};

export const checkPermission = async () => {
  const enabled = await firebase.messaging().hasPermission();
  if (enabled) {
    getFcmToken();
  } else {
    requestPermission();
  }
};

export const messageListener = async navRoute => {
  this.notificationListener = firebase
    .notifications()
    .onNotification(notification => {
      const {title, body, data} = notification;    
      showAlert(title, body, data, navRoute);
    });

  this.notificationOpenedListener = firebase
    .notifications()
    .onNotificationOpened(notificationOpen => {
      const {title, body, data} = notificationOpen.notification;
      showAlert(title, body, data, navRoute);
    });

  const notificationOpen = await firebase
    .notifications()
    .getInitialNotification();
  if (notificationOpen) {
    const {title, body, data} = notificationOpen.notification;
    showAlert(title, body, data, navRoute);
  }

  this.messageListener = firebase.messaging().onMessage(message => {
    console.log(JSON.stringify(message));
  });
};

export const showAlert = (title, message, data, navRoute) => {
  Alert.alert(
    title,
    message,
    [{text: 'OK', onPress: () => console.log('hello')}],
    {cancelable: false},
  );
};

const moveToConfirmation = (navRoute, title, message, data) => {
  const notifParam = {
    title,
    message,
    data
  };
  if (navRoute.state.routeName.toLowerCase() == 'notes' ) {
    navRoute.navigate('Confirm', notifParam);
  } else {
    return;
  }
};
