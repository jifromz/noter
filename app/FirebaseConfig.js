import * as firebaseX from 'firebase';

const config = {
    apiKey: 'AIzaSyB3xtTJrGyFb3UV9r2ZLiS-3M9eTzRwGXo',
    authDomain: 'rnnoter-895fc.firebaseapp.com',
    databaseURL: 'https://rnnoter-895fc.firebaseio.com',
    projectId: 'rnnoter-895fc',
    storageBucket: 'rnnoter-895fc.appspot.com',
    messagingSenderId: '915695315679',
    appId: '1:915695315679:web:50607fa7d745f0f77bd76a',
}

export default  !firebaseX.apps.length ? firebaseX.initializeApp(config) : firebaseX.app();

 
