import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {SignInStyle} from './SignInStyles';
import {readUserData, writeUserData} from '../../ApiServices/Apis';
import {currentUser} from '../../Actions/Users/usersActions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import firebaseX from '../../FirebaseConfig';
import Loader from './../../HelperComponents/Loader';
import {Button, Container} from 'native-base';
import {checkPermission} from '../../util/FCMHelper';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
 

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      errorMessage: null,
      isSaving: false,
      name: '',
      deviceToken: '',
    };
  }

  componentDidMount() {
    checkPermission();
    this.setFCMTokenOnAsyncStorage();
  }

  setFCMTokenOnAsyncStorage = async () => {
    const fcmToken = await firebase.messaging().getToken(); 
    this.setState({
      deviceToken: fcmToken,
    });   
  };

  static navigationOptions = {
    headerShown: false,
  };

  handleSignUp = () => {
    console.log(this.state.deviceToken);
    this.setState({
      isSaving: !this.state.isSaving,
    });
    if (!this.state.confirmPassword == this.state.password) {
      this.setState({
        errorMessage: 'Please retype password.',
        isSaving: !this.state.isSaving,
      });
    } else if (
      this.state.email == '' ||
      this.state.confirmPassword == '' ||
      this.state.password == ''
    ) {
      this.setState({
        errorMessage: 'Please fill-up all the information.',
        isSaving: this.state.isSaving,
      });
    } else {
      let param = {
        email: this.state.email,
        name: this.state.name,
        password: this.state.password,
        deviceToken: this.state.deviceToken,
      };
      firebaseX
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(user => {      
          if (user) {
            user.user
              .updateProfile({
                displayName: param.name,
              })
              .then(s => {
                writeUserData(param).then(data => {
                  firebaseX
                  .auth()
                  .signInWithEmailAndPassword(this.state.email, this.state.password)
                  .then(res => {
                    let user = {
                      email: res.user.email,
                      uid: res.user.uid,
                      displayName: res.user.displayName,
                      refreshToken: res.user.refreshToken,
                    };
                    this.setState(
                      {
                        isSaving: false,
                      },
                      () => {
                        this.props.currentUser(user);
                        this.props.navigation.navigate('Notes', user);
                      },
                    );
                  })
                  .catch(err => {
                    this.setState({
                      isSaving: false,
                      errorMessage: err.message,
                    });
                  });
                });
              });
          }
        })
        .catch(err => {
          this.setState({
            isSaving: false,
            errorMessage: err.message,
          });
        });
    }
  };

  render() {
    return (
      <Container>
        {this.state.isSaving ? (
          <Loader text={'Signing Up...'} />
        ) : (
          <View style={SignInStyle.container}>
            <Text
              style={{
                fontSize: 25,
                fontFamily: 'Montserrat-Black',
                marginBottom: 20,
                color: '#AD1657',
              }}>
              Sign Up
            </Text>
            {this.state.errorMessage && (
              <Text
                style={{
                  color: 'red',
                  textAlign: 'center',
                  marginBottom: 10,
                  width: '90%',
                }}>
                {this.state.errorMessage}
              </Text>
            )}
            <TextInput
              placeholder="Full Name"
              autoCapitalize="none"
              style={SignInStyle.textInput}
              onChangeText={name => this.setState({name})}
              value={this.state.name}
            />
            <TextInput
              placeholder="Email"
              autoCapitalize="none"
              style={SignInStyle.textInput}
              onChangeText={email => this.setState({email})}
              value={this.state.email}
            />
            <TextInput
              secureTextEntry
              placeholder="Password"
              autoCapitalize="none"
              style={SignInStyle.textInput}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
            <TextInput
              secureTextEntry
              placeholder="Confirm Password"
              autoCapitalize="none"
              style={[SignInStyle.textInput, {marginBottom: 10}]}
              onChangeText={confirmPassword => this.setState({confirmPassword})}
              value={this.state.confirmPassword}
            />
            <Button style={SignInStyle.btnStyle} onPress={this.handleSignUp}>
              <Text style={{fontSize: 18, color: '#fff', fontWeight: 'bold'}}>
                Sign Up
              </Text>
            </Button>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 15}}
              onPress={() => this.props.navigation.navigate('SignIn')}>
              <Text style={{fontSize: 16, color: '#AD1657'}}>
                Don't have an account? Sign In
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    createUser: state.createUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUp);
