import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ViewComponent,
} from 'react-native';
import {SignInStyle} from './SignInStyles';
import firebaseX from '../../FirebaseConfig';
import {currentUser} from '../../Actions/Users/usersActions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Button, Container} from 'native-base';
import Loader from './../../HelperComponents/Loader';
import firebase from 'react-native-firebase';
 

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'jif@jif.com',
      password: 'jif12345',
      errorMessage: null,
      isLoading: false,
    };
  }

  static navigationOptions = {
    headerShown: false,
  };

  componentDidMount = async () => {
    const fcmToken = await firebase.messaging().getToken();
  };

  handleSignIn = () => {
    if (this.state.email == '' || this.state.password == '') {
      this.setState({errorMessage: 'Invalid username or passowrd.'});
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          firebaseX
            .auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(res => {
              let user = {
                email: res.user.email,
                uid: res.user.uid,
                displayName: res.user.displayName,
                refreshToken: res.user.refreshToken,
              };
              this.setState(
                {
                  isLoading: false,
                },
                () => {
                  this.props.currentUser(user);
                  this.props.navigation.navigate('Notes', user);
                },
              );
            })
            .catch(err => {
              this.setState({
                isLoading: false,
                errorMessage: err.message,
              });
            });
        },
      );
    }
  };

  render() {
    return (
      <Container>
        {this.state.isLoading ? (
          <Loader text={'Signing In...'} />
        ) : (
          <View style={SignInStyle.container}>
            <Text
              style={{
                fontSize: 25,
                fontFamily: 'Montserrat-Black',
                marginBottom: 20,
                color: '#AD1657',
              }}>
              User Login
            </Text>
            {this.state.errorMessage && (
              <Text
                style={{
                  color: 'red',
                  textAlign: 'center',
                  marginBottom: 10,
                  width: '90%',
                }}>
                {this.state.errorMessage}
              </Text>
            )}
            <TextInput
              style={SignInStyle.textInput}
              autoCapitalize="none"
              placeholder="Email"
              onChangeText={email => this.setState({email})}
              value={this.state.email}
            />
            <TextInput
              secureTextEntry
              style={[SignInStyle.textInput, {marginBottom: 10}]}
              autoCapitalize="none"
              placeholder="Password"
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
            <Button style={SignInStyle.btnStyle} onPress={this.handleSignIn}>
              <Text style={{fontSize: 18, color: '#fff', fontWeight: 'bold'}}>
                Sign In
              </Text>
            </Button>
            <TouchableOpacity
              style={{alignItems: 'center', marginTop: 15}}
              onPress={() => this.props.navigation.navigate('SignUp')}>
              <Text style={{fontSize: 16, color: '#AD1657'}}>
                Don't have an account? Sign Up
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser,
    createUser: state.createUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignIn);
