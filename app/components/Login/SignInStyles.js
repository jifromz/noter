import React from 'react';
import {StyleSheet} from 'react-native';

export const SignInStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: '#AD1657',
    borderWidth: 2,
    marginTop: 8,
  },
  viewStyle: {
    justifyContent: 'center',
    height: '60%',
    width: '95%',
    backgroundColor: '#E0E0E0',
  },
  viewItem1: {
    justifyContent: 'center',
    flex: 1
  },
  viewItem2: {
    justifyContent: 'space-around',
    flex: 2,     
  },
  touchableContainer: {
    width: '60%',
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
    alignItems: 'center',
  },
  btnStyle:{
    width: '90%',
    marginTop: 15,
    marginBottom: 15,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#AD1657',
  },
  viewItem3: {
    justifyContent: 'center',
    flex: 1,
    justifyContent: 'space-around',
   flexDirection: 'column'
  },
  viewItemBody:{   
    justifyContent: 'space-around'
  },
  formStyle: {
    alignSelf: 'stretch'
  }
});
