/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import SignIn from './SignIn';
import SignUp from './SignUp';

const AuthStack = createStackNavigator(
  {SignIn: SignIn, SignUp: SignUp},
  {
    headerLayoutPreset: 'center',
  },
  {initialRouteName: 'SignIn'},
);

AuthStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const AuthContainer = createAppContainer(AuthStack);

export default AuthContainer;
