import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';
import Loader from '../../HelperComponents/Loader';
import {currentUser} from '../../Actions/Users/usersActions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
let timer = null
class LoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    
  }

  componentDidMount() {     
    timer = setTimeout(() => {
      this.props.navigation.navigate('Auth');
    //   this.props.navigation.navigate(user ? 'App' : 'Auth');
    }, 1000);
    // firebaseX.auth().onAuthStateChanged(u => {

    //   console.log(u);
    //   let user = {
    //     email: u.email,
    //     uid: u.uid,
    //     displayName: u.displayName,
    //     refreshToken: u.refreshToken
    //   }
      
    // //  u.ref().remove();
    //   this.props.currentUser(user);
     
  //  })
  }

  componentWillUnmount(){
    clearTimeout(timer);
  }  

  render() {
    return (
      <Loader text={'Loading...'} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoadingScreen);


