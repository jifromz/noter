import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {Title, Container, Button, Header, Content} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        require('../../../assets/StoriesHeaderThumbnails/1.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/2.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/3.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/4.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/5.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/6.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/7.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/8.jpg'),
      ],
    };
  }

  randomImage = () => {
    let num = Math.floor(Math.random() * this.state.images.length);
    let img = this.state.images[num];
    return (
      <Image
        source={img}
        style={{
          width: 150,
          height: 150,
          borderRadius: 150 / 2,
          borderWidth: 2,
          borderColor: '#AD1457',
          marginHorizontal: 5,
        }}
      />
    );
  };

  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#AD1457',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Title style={{color: '#fff', fontFamily: 'Montserrat-Bold'}}>
            Profile
          </Title>
        </Header>
        <Content
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          }}>
          <View
            style={{flex: 4, alignItems: 'center', justifyContent: 'center'}}>
            {this.randomImage()}
            <Text
              style={{
                marginTop: 30,
                fontSize: 20,
                marginBottom: 10,
                fontWeight: 'bold',
                textAlign: 'center',
                fontFamily: 'Montserrat-Black',
                color: '#AD1457',
              }}>
              {this.props.currUser.currentUser.displayName
                ? this.props.currUser.currentUser.displayName
                : 'Jifford R. Romasanta'}
            </Text>
            <Text
              style={{
                fontSize: 17,
                textAlign: 'center',

                fontWeight: '900',
                color: '#616161',
              }}
              note>
              {this.props.currUser.currentUser.email}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              width: '100%',
            }}>
            <Button
              full
              style={styles.btnStyle}
              onPress={() => this.props.navigation.navigate('SignIn')}>
              <Text
                style={{fontSize: 15, color: '#FFFFFF', fontWeight: 'bold'}}>
                Logout
              </Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  btnStyle: {
    width: '90%',
    marginBottom: 10,
    alignSelf: 'center',
    backgroundColor: '#AD1657',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  console.log(state);
  return {
    currUser: state.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
