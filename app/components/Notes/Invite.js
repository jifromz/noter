import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Icon,
  List,
  ListItem,
  Body,
  Right,
  Content,
  Container,
  Button,
  Header,
  Left,
  Title,
  Thumbnail
} from 'native-base';
import {
  readUsersData,
  writeAttendanceData,
  readAttendanceData,
} from '../../ApiServices/Apis';
import Loader from '../../HelperComponents/Loader';

export default class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      headerTitle: 'Invite',
      noteKey: '',
      attendance: [],
      isLoading: true,
      images: [
        require('../../../assets/StoriesHeaderThumbnails/1.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/2.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/3.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/4.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/5.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/6.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/7.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/8.jpg'),
      ],
    };
  }

  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};
    return {
      title: params.noteSubject,
      headerTintColor: '#ffffff',
      headerStyle: {
        backgroundColor: '#AD1657',
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1,
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Bold',
      },
    };
  };

  randomImage = () => {
    let num = Math.floor(Math.random() * this.state.images.length);
    let img = this.state.images[num];
    return (
      <Thumbnail
        style={{
          borderWidth: 2,
          borderColor: '#AD1657',
          marginHorizontal: 5,
        }}
        source={img}
      />
    );
  };

  componentDidMount() {
    let noteKey = this.props.navigation.state.params.noteKey;
    readAttendanceData(noteKey).then(eData => {
      readUsersData().then(data => {
        let usersArr = [];

        for (const key of Object.keys(data)) {
          let user = {};

          let newData = eData ? eData.Users[key] : null;

          // console.log(newData);

          user.Name = data[key].Name;
          user.Key = key;
          user.Email = data[key].Email;
          user.Device_Token = data[key].Device_Token;
          user.IsInvited = eData && eData.Users[key] ? true : false;
          user.HasConfirmed =
            newData != undefined ? eData.Users[key].HasConfirmed : false;
          usersArr.push(user);
        }
        this.setState(
          {
            users: usersArr,
            noteKey: noteKey,
          },
          () => {
            this.setState({
              isLoading: false,
            });
          },
        );
      });
    });
  }

  createAttendance = () => {
    let invitedArr = [];

    this.setState({isLoading: !this.state.isLoading});

    this.state.users.forEach(u => {
      let userObj = {
        User_Id: '',
        HasConfirmed: u.HasConfirmed,
        Device_Token: u.Device_Token,
      };
      if (u.IsInvited) {
        userObj.User_Id = u.Key;
        invitedArr.push(userObj);
      }
    });

    let obj = [];

    for (let index = 0; index < invitedArr.length; index++) {
      const element = invitedArr[index];

      console.log(element);

      let attendees = {};
      attendees['HasConfirmed'] = element.HasConfirmed;
      attendees['Device_Token'] = element.Device_Token;
      obj[element.User_Id] = attendees;
    }

    let param = {
      Subject: this.props.navigation.state.params.noteSubject,
      Description: this.props.navigation.state.params.noteDescription,
      Note_Id: this.props.navigation.state.params.noteKey,
      Users: obj,
      IsUpdate: false,
    };

    writeAttendanceData({id: this.state.noteKey, obj: param}).then(data => {
      this.setState({isLoading: !this.state.isLoading});
    });
  };

  inviteUser = key => {
    let users = this.state.users;
    let userIndex = users.findIndex(u => u.Key == key);
    users[userIndex].IsInvited = !users[userIndex].IsInvited;
    this.setState({
      users: users,
    });
  };

  render() {
    return (
      <Container>
        {this.state.isLoading ? (
          <Loader text={'Loading...'} />
        ) : (
          <ScrollView>
            <List style={styles.list}>
              {this.state.users.map(user => {
                return (
                  <ListItem
                    key={user.Key}
                    style={{marginLeft: 0, paddingLeft: 5}}>
                    <Left style={{flex: 1}}>
                      {this.randomImage()}
                    </Left>
                    <Body style={{flex: 5, paddingLeft: 20}}>
                      <View
                        style={{
                          flexDirection: 'column',
                          justifyContent: 'space-around',
                        }}>
                        <Text style={{fontSize: 17}}>{user.Name}</Text>
                        <Text style={{color: '#9E9E9E'}}>{user.Email}</Text>
                      </View>
                    </Body>
                    <Right style={{flex: 1, justifyContent: 'flex-end'}}>                    
                        <TouchableOpacity
                          style={styles.touchable}
                          onPress={() => {
                            this.inviteUser(user.Key);
                          }}>
                          <Icon
                            name={
                              user.IsInvited
                                ? 'md-checkmark-circle'
                                : 'md-close-circle'
                            }
                            style={[
                              styles.iconStyle,
                              {
                                color: user.IsInvited ? '#2E7D32' : '#9E9E9E',
                                fontSize: 27,
                              },
                            ]}
                          />
                          <Text style={{fontSize: 11, textAlign: 'center'}}>
                            {!user.IsInvited ? 'Invite' : 'Invited'}
                          </Text>
                        </TouchableOpacity>                      
                    </Right>
                  </ListItem>
                );
              })}
            </List>
          </ScrollView>
        )}
        <Button
          full
          style={styles.addButton}
          onPress={() => this.createAttendance()}>
          <Text style={{fontSize: 17, color: '#FFFFFF', fontWeight: 'bold'}}>
            Invite
          </Text>
        </Button>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableContainer: {
    width: '60%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconStyle: {
    color: '#2196F3',
    fontSize: 22,
    alignSelf: 'center',
  },
  list: {
    flex: 1,
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  },
  addButton: {
    width: '98%',
    marginBottom: 10,
    marginTop: 5,
    alignSelf: 'center',
    backgroundColor: '#AD1657',
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});
