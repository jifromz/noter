import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {Content, Button} from 'native-base';
import {
  readNotesDataByNoteId,
  readAttendanceData,
  readUsersData,
  updateAttendance
} from '../../ApiServices/Apis';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class Confirm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      noteData: null,
      users: [],
      currentUserEmail: '',
      currentUserKey: '',
    };
  }

  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};
    return {
      title: 'Confirm',
      headerTintColor: '#ffffff',
      headerStyle: {
        backgroundColor: '#AD1657',
        textAlign: 'left',
        alignSelf: 'center',
        flex: 1,
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Bold',
      },
    };
  };

  componentDidMount() {   
    const {NoteKey} = this.props.navigation.state.params;
    this.getUsersData();
    this.getNotesByNoteId(NoteKey);
  }

  getUsersData = () => {
    readUsersData().then(users => {
      let newUsers = Object.keys(users);
      for (const key of newUsers) {
        if (users[key].Email == this.props.currUser.currentUser.email) {
          this.setState({
            currentUserKey: key,
          });
        }
      }
    });
  };

  getNotesByNoteId = noteId => {
    readNotesDataByNoteId(noteId).then(data => {
      //  console.log(data);
      if (data) {
        this.setState({noteData: data, isLoading: false});
      } else {
        this.setState({isLoading: false});
      }
    });
  };

  updateAttendance = noteId => {
    readAttendanceData(noteId).then(data => {           
      let newUsers = Object.keys(data.Users);
     // console.log(newUsers);      
      for (const key of newUsers) {       
        if (key == this.state.currentUserKey) {         
         data.Users[key].HasConfirmed = true;
        }
      }
  
      console.log(data.Users[this.state.currentUserKey])
  
      const {NoteKey} = this.props.navigation.state.params;
  
      let updatedAttendance = {data: data.Users[this.state.currentUserKey], Key: NoteKey, userKey: this.state.currentUserKey};
  
      updateAttendance(updatedAttendance).then(data => {
      //  console.log(data)
        Alert.alert("Joined successfully.");
      })
    });  
  }

  render() {
    return (
      <Content
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
          height: '100%',
        }}>
        {this.state.noteData ? (
          <View style={{flex: 1, width: '100%'}}>
            <View style={styles.txtContainerViewStyle}>
              <Text style={styles.txtStyle}>
                Created By: {this.state.noteData.Created_By}
              </Text>
              <Text style={styles.txtStyle}>
                Email: {this.state.noteData.Email}
              </Text>
              <Text style={styles.txtStyle}>
                Date Start: {this.state.noteData.Date_Start}
              </Text>
              <Text style={styles.txtStyle}>
                Date End: {this.state.noteData.Date_End}
              </Text>
              <Text style={styles.txtStyle}>
                Subject: {this.state.noteData.Subject}
              </Text>
              <Text style={styles.txtStyle}>
                Description: {this.state.noteData.Description}
              </Text>
            </View>
            <View style={styles.txtStyle}>
              <Button
             // disabled={this.state.noteData.Users &&this.state.noteData.Users[this.state.currentUserKey] ? this.state.noteData.Users[this.state.currentUserKey].HasConfirmed : false}
                full
                style={styles.btnStyle}
                onPress={() => this.updateAttendance(this.props.navigation.state.params.NoteKey)}>
                <Text
                  style={{fontSize: 15, color: '#FFFFFF', fontWeight: 'bold'}}>
                  Join
                </Text>
              </Button>
            </View>
          </View>
        ) : (
          <View />
        )}
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  btnStyle: {
    width: '96%',
    marginBottom: 10,
    alignSelf: 'center',
    backgroundColor: '#AD1657',
    alignItems: 'center',
  },
  txtStyle: {
    fontSize: 18,
    textAlign: 'left',
    fontWeight: '900',
    color: '#424242',
    marginBottom: 15,
  },
  txtContainerViewStyle: {
    width: '100%',
    //  backgroundColor: 'red',
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    //paddingLeft: 25,  
  },
});

function mapStateToProps(state) {
  return {
    currUser: state.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Confirm);
