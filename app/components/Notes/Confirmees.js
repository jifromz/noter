import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Icon,
  List,
  ListItem,
  Body,
  Right,
  Content,
  Container,
  Button,
  Header,
  Left,
  Title,
  Thumbnail,
} from 'native-base';
import {
  readUsersData,
  writeAttendanceData,
  readAttendanceData,
} from '../../ApiServices/Apis';
import Loader from '../../HelperComponents/Loader';

export default class Confirmees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      headerTitle: 'Confirmees',
      noteKey: '',
      attendance: [],
      isLoading: true,
      images: [
        require('../../../assets/StoriesHeaderThumbnails/1.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/2.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/3.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/4.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/5.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/6.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/7.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/8.jpg'),
      ],
    };
  }

  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};
    return {
      title: params.noteSubject,
      headerTintColor: '#ffffff',
      headerStyle: {
        backgroundColor: '#AD1657',
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1,
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Bold',
      },
    };
  };

  randomImage = () => {
    let num = Math.floor(Math.random() * this.state.images.length);
    let img = this.state.images[num];
    return (
      <Thumbnail
        style={{
          borderWidth: 2,
          borderColor: '#AD1657',
          marginHorizontal: 5,
        }}
        source={img}
      />
    );
  };

  componentDidMount() {
    let noteKey = this.props.navigation.state.params.noteKey;
    readAttendanceData(noteKey).then(attendance => {
      readUsersData().then(userData => {
        let userKeyArr = [];
        for (const key of Object.keys(attendance.Users)) {
          userKeyArr.push(key);
        }

        let usersArr = [];

        for (let index = 0; index < userKeyArr.length; index++) {
          const userKey = userKeyArr[index];
          if (attendance.Users[userKey].HasConfirmed) {
            usersArr.push(userData[userKey]);
          }
        }

        this.setState({users: usersArr, isLoading: !this.state.isLoading});
      });
    });
  }

  render() {
    return (
      <Container>       
          {this.state.users.length == 0 && !this.state.isLoading && (
            <View
              style={{
                flex: 1,
                width: '100%',
                height: '100%',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  color: '#AD1457',
                  fontSize: 18,
                  fontFamily: 'Montserrat-Regular',
                  fontWeight: 'bold',
                }}>
                No Confirmees
              </Text>
            </View>
          )}
          {this.state.isLoading ? (
            <Loader text={'Loading...'} />
          ) : (
            <ScrollView>
              <List style={styles.list}>
                {this.state.users.map(user => {
                  return (
                    <ListItem
                      key={user.Device_Token}
                      style={{marginLeft: 0, paddingLeft: 5}}>
                      <Left style={{flex: 1}}>{this.randomImage()}</Left>
                      <Body style={{flex: 5, paddingLeft: 20}}>
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'space-around',
                          }}>
                          <Text style={{fontSize: 17}}>{user.Name}</Text>
                          <Text style={{color: '#9E9E9E'}}>{user.Email}</Text>
                        </View>
                      </Body>
                      <Right style={{flex: 1}}>
                        <View style={styles.touchableContainer}>
                          <Icon
                            name={'md-checkmark'}
                            style={[
                              styles.iconStyle,
                              {
                                color: '#2E7D32',
                                fontSize: 30,
                              },
                            ]}
                          />
                        </View>
                      </Right>
                    </ListItem>
                  );
                })}
              </List>
            </ScrollView>
          )}
       
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableContainer: {
    width: '60%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconStyle: {
    color: '#2196F3',
    fontSize: 22,
  },
  list: {
    flex: 1,
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  },
  addButton: {
    width: '96%',
    marginBottom: 10,
    alignSelf: 'center',
    backgroundColor: '#AD1657',
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});
