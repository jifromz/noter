/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Notes from './Notes';
import Note from './Note';
import Invite from './Invite'; 
import Confirmees from './Confirmees' ;
import Confirm from './Confirm';


const AppStack = createStackNavigator(
  {
    Notes: {
      screen: Notes,
      navigationOptions: {
        headerTitleStyle: {textAlign: 'center', alignSelf: 'center',  fontFamily: 'Montserrat-Bold' },
      },
    },
    Note,
    Invite,     
    Confirmees,
    Confirm
  },
  {initialRouteName: 'Notes', headerLayoutPreset: 'center'},
);

const NoteContainer = createAppContainer(AppStack);

export default NoteContainer;
