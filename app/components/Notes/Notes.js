import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Icon,
  List,
  ListItem,
  Body,
  Right,
  Content,
  Container,
  Fab,
} from 'native-base';
import Loader from '../../HelperComponents/Loader';
import {readNotesData, deleteNote} from '../../ApiServices/Apis';
import {checkPermission, messageListener} from '../../util/FCMHelper';

export default class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: [],
      isLoading: true,
      active: true,
      navigatedAway: false,
    };
  }

  static navigationOptions = {
    title: 'Notes',
    headerTintColor: '#ffffff',
    headerStyle: {
      backgroundColor: '#AD1457',
    },
    headerTitleStyle: {
      flex: 1,
      textAlign: 'center',
    },
  };

  componentDidMount() {
    console.log(this.props.navigation);
    // checkPermission();
    messageListener(this.props.navigation);
    this.fetchNotes();
  }

  handleOnNavigateBack = () => {
    this.setState(
      {
        isLoading: true,
      },
      () => {
        this.fetchNotes();
      },
    );
  };

  fetchNotes() {
    readNotesData().then(data => {
      let noteArr = [];
      if (data) {
        for (const key of Object.keys(data)) {
          let obj = data[key];
          obj.Key = key;
          noteArr.push(obj);
        }
      }

      this.setState(
        {
          notes: noteArr,
        },
        () => {
          this.setState({
            isLoading: false,
          });
        },
      );
    });
  }

  deleteNote = Key => {
    deleteNote(Key).then(() => {
      let arr = [];
      this.state.notes.forEach(i => {
        if (i.Key !== Key) {
          arr.push(i);
        }
      });
      this.setState({
        notes: arr,
      });
    });
  };

  createdByCurrentUser(note) {
    return !this.props.navigation.state.params
      ? false
      : this.props.navigation.state.params.email === note.Email;
  }

  render() {
    return (
      <Container>
        {this.state.notes.length == 0 && !this.state.isLoading && (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                alignSelf: 'center',
                color: '#AD1457',
                fontSize: 18,
                fontFamily: 'Montserrat-Regular',
                fontWeight: 'bold',
              }}>
              No Notes Available
            </Text>
          </View>
        )}
        {this.state.isLoading && <Loader text={'Loading Notes'} />}
        {this.state.notes.length > 0 && !this.state.isLoading && (
          <ScrollView>
            {this.state.notes.length > 0 && !this.state.isLoading && (
              <List style={styles.list}>
                {this.state.notes.map(note => {
                  return (
                    <ListItem
                      key={note.Key}
                      style={{marginLeft: 0, paddingLeft: 15}}>
                      <Body>
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'space-around',
                          }}>
                          <Text style={{fontSize: 17}}>{note.Subject}</Text>
                          <Text style={{color: '#9E9E9E'}}>
                            {note.Created_By}
                          </Text>
                        </View>
                      </Body>
                      <Right>
                        <View
                          style={[
                            styles.touchableContainer,
                            {
                              justifyContent: this.createdByCurrentUser(note)
                                ? 'space-around'
                                : 'flex-end',
                            },
                          ]}>
                          {this.createdByCurrentUser(note) ? (
                            <TouchableOpacity
                              style={styles.touchable}
                              onPress={() => {
                                this.deleteNote(note.Key);
                              }}>
                              <Icon
                                name="md-trash"
                                style={[styles.iconStyle, {color: '#9E9E9E'}]}
                              />
                            </TouchableOpacity>
                          ) : null}
                          <TouchableOpacity
                            style={styles.touchable}
                            onPress={() => {
                              this.setState({navigate: true}, () => {
                                this.props.navigation.navigate('Note', {
                                  note,
                                  IsNew: false,
                                  handleOnNavigateBack: this
                                    .handleOnNavigateBack,
                                });
                              });
                            }}>
                            <Icon
                              name="arrow-forward"
                              style={[styles.iconStyle, {fontSize: 24}]}
                            />
                          </TouchableOpacity>
                        </View>
                      </Right>
                    </ListItem>
                  );
                })}
              </List>
            )}
          </ScrollView>
        )}
        <Fab
          active={this.state.active}
          direction="up"
          style={{backgroundColor: '#AD1457'}}
          position="bottomRight"
          onPress={() => {
            this.props.navigation.navigate('Note', {
              count: this.state.notes.length,
              Subject: 'Add Note',
              IsNew: true,
              handleOnNavigateBack: this.handleOnNavigateBack,
            });
          }}>
          <Icon style={{fontSize: 20}} name="md-add" />
        </Fab>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableContainer: {
    width: '90%',
    flex: 2,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    color: '#AD1457',
    fontSize: 22,
  },
  list: {
    flex: 1,
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: 'red',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});
