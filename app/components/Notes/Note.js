import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Platform,
  TouchableOpacity,
  ScrollView,
  Alert,
  // AsyncStorage,
} from 'react-native';
import {
  Item,
  Input,
  Content,
  Label,
  Form,
  Button,
  Container,
  Textarea,
  Text,
  Icon,
} from 'native-base';
import Header from '../../HelperComponents/Header';

import Loader from '../../HelperComponents/Loader';
import Moment from 'moment';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {writeNoteData, updateNoteData} from '../../ApiServices/Apis';
import BackButton from '../../HelperComponents/HeaderbackButton';
import DateTimePicker from '@react-native-community/datetimepicker';

class Note extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Created_By: '',
      Date_End: '',
      Date_Start: '',
      Description: '',
      Subject: '',
      chosenStartDate: null,
      chosenStartTime: '',
      chosenDueDate: null,
      chosenDueTime: '',
      strStartDate: null,
      strDueDate: null,
      showTimepicker: false,
      showDatepicker: false,
      isStartDate: true,
      isLoading: false,
      btnTitle: 'Save',
      errorMessage: '',
      count: 0,
      date: new Date(),
      show: false,
      showMode: 'date',
      chosenKey: 'chosenStartDate',
      Key: '',
    };
  }

  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};
    return {
      headerLeft: (
        <BackButton
          onPress={() => {
            params.handleOnNavigateBack();
            navigation.goBack();
          }}
        />
      ),
      title: params.IsNew ? 'Add Note' : params.note.Subject,
      headerTintColor: '#ffffff',
      headerStyle: {
        backgroundColor: '#AD1657',
        textAlign: 'center',
        alignSelf: 'center',
        flex: 1,
      },
      headerTitleStyle: {
        fontFamily: 'Montserrat-Bold',
      },
    };
  };

  componentDidMount = () => {
    const {params} = this.props.navigation.state;
    console.log(params);
    if (params.note && !params.IsNew) {
      this.setState({
        Created_By: params.note.Created_By,
        Date_End: params.note.Date_End,
        Date_Start: params.note.Date_Start,
        Description: params.note.Description,
        Subject: params.note.Subject,
        btnTitle: params.note.Key ? 'Update' : 'Save',
        count: params.count,
        Key: params.note.Key,
      });
    } else {
      this.setState({
        Created_By: this.props.currUser.currentUser.displayName,
      });
    } 
  };

  createdByCurrentUser() {
    return (
      this.props.navigation.state.params.IsNew ||
      this.props.navigation.state.params.note.Email.toLowerCase() ==
        this.props.currUser.currentUser.email.toLowerCase()
    );
  }

  handleWriteNote = () => {
    if (
      !this.state.Date_End ||
      0 === this.state.Date_End.length ||
      !this.state.Date_Start ||
      0 === this.state.Date_Start.length ||
      !this.state.Description ||
      0 === this.state.Description.length ||
      !this.state.Subject ||
      0 === this.state.Subject.length
    ) {
      this.setState(
        {
          errorMessage: 'Please fill-up all the information.',
        },
        () => {
          Alert.alert("Error",this.state.errorMessage);
        },
      );
    } else {
      this.setState({isLoading: !this.state.isLoading}, () => {
        let param = {
          Created_By: this.props.navigation.state.params.IsNew
            ? this.props.currUser.currentUser.displayName
            : this.state.Created_By,
          Date_End: this.state.Date_End,
          Date_Start: this.state.Date_Start,
          Description: this.state.Description,
          Subject: this.state.Subject,
          Email: this.props.currUser.currentUser.email,
        };

        if (this.state.Key) {
          param.Key = this.state.Key;
          updateNoteData(param).then(data => {
            this.setState({
              isLoading: false,
            });
          });
        } else {
          writeNoteData(param).then(data => {
            this.setState({
              Created_By: '',
              Date_End: '',
              Date_Start: '',
              Description: '',
              Subject: '',
              isLoading: false,
            });
          });
        }
      });
    }
  };

  showDatepicker = (isStartDate, show) => {
    this.setState({
      isStartDate: isStartDate,
      showMode: 'date',
      show: show,
    });
  };

  showTimepicker = (isStartTime, show) => {
    this.setState({
      isStartTime: isStartTime,
      showMode: 'time',
      show: show,
    });
  };

  onChangeDateTime = (event, selectedDate) => {
    if (selectedDate == null || selectedDate == undefined) {
      return;
    }

    if (this.state.showMode === 'date') {
      console.log(selectedDate);
      let date = Moment(selectedDate, 'YYYY/MM/DD');
      if (this.state.isStartDate) {
        this.setState(
          {
            chosenStartDate: this.dateFormatter(date),
            show: !this.state.show,
          },
          () => {
            this.setState({
              Date_Start: this.state.chosenStartDate,
            });
          },
        );
      } else {
        this.setState(
          {
            chosenDueDate: this.dateFormatter(date),
            show: !this.state.show,
          },
          () => {
            this.setState({
              Date_End: this.state.chosenDueDate,
            });
          },
        );
      }
    }

    if (this.state.showMode === 'time') {
      let newDate = Moment(new Date(), 'YYYY/MM/DD');
      if (this.state.isStartTime) {
        this.setState(
          {
            chosenStartTime: selectedDate
              ? this.timeFormatter(selectedDate).toString()
              : this.timeFormatter(newDate).toString(),
            show: !this.state.show,
          },
          () => {
            let newStartDate = this.state.Date_Start.split(' ')[0];
            let newStartTime = this.state.chosenStartTime;
            this.setState({
              Date_Start: newStartDate + ' ' + newStartTime,
            });
          },
        );
      } else {
        this.setState(
          {
            chosenDueTime: selectedDate
              ? this.timeFormatter(selectedDate).toString()
              : this.timeFormatter(newDate).toString(),
            show: !this.state.show,
          },
          () => {
            let newEndDate = this.state.Date_End.split(' ')[0];
            let newEndTime = this.state.chosenDueTime;
            this.setState({
              Date_End: newEndDate + ' ' + newEndTime,
            });
          },
        );
      }
    }
  };

  timeFormatter = dateParam => {
    let time = Moment(dateParam, 'HH:mm');
    console.log(time.format('HH') + ':' + time.format('mm A'));
    return time.format('HH') + ':' + time.format('mm A');
  };

  dateFormatter = dateParam => {
    return (
      dateParam.format('MM') +
      '-' +
      dateParam.format('DD') +
      '-' +
      dateParam.format('YYYY')
    );
  };

  render() {
    return (
      <Container>
        {this.state.show && (
          <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={this.state.date}
            mode={this.state.showMode}
            is24Hour={false}
            display="default"
            onChange={this.onChangeDateTime}
          />
        )}
        {this.state.isLoading && <Loader text={'Updating Data'} />}
        {!this.state.isLoading && (
          <ScrollView
            contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
            <View style={styles.form}>
              <Item inlineLabel style={{marginRight: 10}}>
                <Label style={{fontSize: 16}}>Subject: </Label>
                <Input
                  style={{fontSize: 16}}
                  returnKeyType="next"
                  autoCorrect={false}
                  onChangeText={Subject => this.setState({Subject})}
                  value={this.state.Subject}
                  disabled={!this.createdByCurrentUser()}
                />
              </Item>
              <Item inlineLabel style={{marginRight: 10}}>
                <Label style={{fontSize: 16}}>Created By: </Label>
                <Input
                  style={{fontSize: 16}}
                  returnKeyType="next"
                  autoCorrect={false}
                  onChangeText={Created_By => this.setState({Created_By})}
                  value={this.state.Created_By}
                  disabled={!this.createdByCurrentUser()}
                />
              </Item>
              <Item inlineLabel style={{marginRight: 10}}>
                <Label style={{fontSize: 16}}>Date Start: </Label>
                <Input
                  style={{fontSize: 16}}
                  returnKeyType="next"
                  autoCorrect={false}
                  onChangeText={Date_Start => this.setState({Date_Start})}
                  value={this.state.Date_Start.toString()}
                  disabled={!this.createdByCurrentUser()}
                />
                <View style={styles.touchableContainer}>
                  <TouchableOpacity
                    style={styles.touchable}
                    onPress={() => this.showDatepicker(true, true)}>
                    <Icon name="ios-calendar" style={styles.iconStyle} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.touchable}
                    onPress={() => this.showTimepicker(true, true)}>
                    <Icon
                      ios="ios-clock"
                      android="md-clock"
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                </View>
              </Item>
              <Item inlineLabel style={{marginRight: 10}}>
                <Label style={{fontSize: 16}}>Date End: </Label>
                <Input
                  style={{fontSize: 16}}
                  returnKeyType="next"
                  autoCorrect={false}
                  onChangeText={Date_End => this.setState({Date_End})}
                  value={this.state.Date_End.toString()}
                  disabled={!this.createdByCurrentUser()}
                />
                <View style={styles.touchableContainer}>
                  <TouchableOpacity
                    style={styles.touchable}
                    onPress={() => this.showDatepicker(false, true)}>
                    <Icon name="ios-calendar" style={styles.iconStyle} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.touchable}
                    onPress={() => this.showTimepicker(false, true)}>
                    <Icon
                      ios="ios-clock"
                      android="md-clock"
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                </View>
              </Item>
              <Textarea
                style={{
                  width: '98%',                
                  fontSize: 18,
                  marginTop: 10,
                  marginBottom: 10,
                  marginLeft: 0,
                }}
                rowSpan={4}
                placeholder="Description"
                bordered
                value={this.state.Description}
                onChangeText={Description => this.setState({Description})}
                autoCorrect={false}
                disabled={!this.createdByCurrentUser()}
              />
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'space-around',
                alignItems: 'center',               
              }}>
              {this.createdByCurrentUser() && (
                <Button
                  full
                  style={styles.btnStyle}
                  onPress={() => this.handleWriteNote()}>
                  <Text style={styles.btnText}>{this.state.btnTitle}</Text>
                </Button>
              )}
              <Button
                disabled={this.props.navigation.state.params.IsNew}
                full
                style={[
                  styles.btnStyle,
                  {
                    backgroundColor: this.props.navigation.state.params.IsNew
                      ? '#EEEEEE'
                      : '#AD1657',
                  },
                ]}
                onPress={() =>
                  this.props.navigation.navigate('Invite', {
                    noteKey: this.state.Key,
                    noteSubject: this.state.Subject,
                    noteDescription: this.state.Description,
                    userEmail: this.props.currUser.currentUser.email,
                  })
                }>
                <Text
                  style={[
                    styles.btnText,
                    {
                      color: this.props.navigation.state.params.IsNew
                        ? '#9E9E9E'
                        : 'white',
                    },
                  ]}>
                  Invite
                </Text>
              </Button>
              <Button
                disabled={this.props.navigation.state.params.IsNew}
                full
                style={[
                  styles.btnStyle,
                  {
                    backgroundColor: this.props.navigation.state.params.IsNew
                      ? '#EEEEEE'
                      : '#AD1657',
                  },
                ]}
                onPress={() =>
                  this.props.navigation.navigate('Confirmees', {
                    noteKey: this.state.Key,
                    noteSubject: this.state.Subject,
                  })
                }>
                <Text
                  style={[
                    styles.btnText,
                    {
                      color: this.props.navigation.state.params.IsNew
                        ? '#9E9E9E'
                        : 'white',
                    },
                  ]}>
                  Confirmees
                </Text>
              </Button>
            </View>
          </ScrollView>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  updateBtnContainer: {flex: 1},
  btnText: {fontSize: 15, color: '#ffffff'},
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scroller: {
    flex: 1,
    flexDirection: 'column',
    height: '100%',
  },
  btnStyle: {
    width: '96%',
    alignSelf: 'center',
    backgroundColor: '#AD1657',
    marginBottom: 5,
  },
  form: {
    marginBottom: 0,
    marginLeft: 0,
    paddingLeft: 10,
    paddingRight: 0,
    marginRight: 0,
    flex: 2,
    justifyContent: 'space-around',
  },
  touchable: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  touchableContainer: {
    flex: 0.5,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    color: '#9E9E9E',
    fontSize: 22,
  },
});

function mapStateToProps(state) {
  console.log(state);
  return {
    currUser: state.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Note);
