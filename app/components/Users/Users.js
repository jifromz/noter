import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Icon,
  List,
  ListItem,
  Body,
  Right,
  Content,
  Container,
  Fab,
  Header,
  Left,
  Title,
  Thumbnail,
} from 'native-base';
import {readUsersData, deleteUser} from '../../ApiServices/Apis';
import Loader from '../../HelperComponents/Loader';

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      images: [
        require('../../../assets/StoriesHeaderThumbnails/1.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/2.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/3.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/4.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/5.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/6.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/7.jpg'),
        require('../../../assets/StoriesHeaderThumbnails/8.jpg'),
      ],
    };
  }

  componentDidMount() {
    readUsersData().then(data => {
      let usersArr = [];

      for (const key of Object.keys(data)) {
        let obj = data[key];
        obj.Key = key;
        usersArr.push(obj);
      }

      this.setState(
        {
          users: usersArr,
        },
        () => {
          this.setState({
            isLoading: false,
          });
        },
      );
    });
  }

  deleteUser = Key => {
    deleteUser(Key).then(() => {
      let arr = [];
      this.state.users.forEach(i => {
        if (i.Key != Key) {
          arr.push(i);
        }
      });
      this.setState({
        users: arr,
      });
    });
  };

  randomImage = () => {
    let num = Math.floor(Math.random() * this.state.images.length);
    let img = this.state.images[num];
    return (
      <Thumbnail
        style={{
          borderWidth: 2,
          borderColor: '#AD1657',
          marginHorizontal: 5,
        }}
        source={img}
      />
    );
  };

  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#AD1457',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Title style={{color: '#fff', fontFamily: 'Montserrat-Bold'}}>
            Users
          </Title>
        </Header>
        <ScrollView>
          {this.state.isLoading ? (
            <Loader />
          ) : (
            <List style={styles.list}>
              {this.state.users.map(user => {
                return (
                  <ListItem key={user.Key} style={{marginLeft: 5}}>
                    <Left style={{flex: 1}}>{this.randomImage()}</Left>
                    <Body style={{flex: 5, paddingLeft: 10}}>
                      <View
                        style={{
                          flexDirection: 'column',
                          justifyContent: 'space-around',
                        }}>
                        <Text style={{fontSize: 18}}>{user.Name}</Text>
                        <Text note style={{fontSize: 15, color: '#9E9E9E'}}>
                          {user.Email}
                        </Text>
                      </View>
                    </Body>
                  </ListItem>
                );
              })}
            </List>
          )}
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableContainer: {
    width: '60%',
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconStyle: {
    color: '#2196F3',
    fontSize: 22,
  },
  list: {
    flex: 1,
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: 'red',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});
