import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  Icon,
  List,
  ListItem,
  Body,
  Right,
  Content,
  Container,
  Button,
  Header,
  Left,
  Title,
} from 'native-base';
import {
  readUsersData,
  readAttendanceData,
  readAllAttendanceData,
} from '../ApiServices/Apis';
import Loader from '../HelperComponents/Loader';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class Invites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invites: [],
      noteKey: '',
      isLoading: true,
    };
  }

  handleOnNavigateBack = () => {
    readAllAttendanceData().then(eData => {
      if (eData) {
        readUsersData().then(users => {
          let currentUser = '';
          for (const key of Object.keys(users)) {

            let user = users[key];

            if (user.Email === this.props.currUser.currentUser.email) {
              currentUser = key;
            }
          }

          const invites = Object.values(eData);
          let invitations = [];
          for (let data in invites) {
            let keys = Object.keys(invites[data].Users);         
            for (let index = 0; index < keys.length; index++) {
              const element = keys[index];
              if (element == currentUser) {           
                let noteObj = {
                  Subject: invites[data].Subject,
                  Description: invites[data].Description,
                  NoteKey: invites[data].Note_Id,
                };
                invitations.push(noteObj);
              }
            }
          }
          this.setState({
            invites: invitations,
            isLoading: false
          });
        });
       // this.setState({isLoading: false});
      } else {
        this.setState({
          isLoading: !this.state.isLoading,
        });
      }
    });
  };

  componentDidMount() {
    this._navListener = this.props.navigation.addListener('didFocus', () => {
      this.setState({isLoading: true}, () => this.handleOnNavigateBack());
    });
  }

  isLoa;

  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#AD1457',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Title style={{color: '#fff', fontFamily: 'Montserrat-Bold'}}>
            My Invitations
          </Title>
        </Header>
        <View style={styles.container}>
          {this.state.invites.length == 0 && !this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                width: '100%',
                height: '100%',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  color: '#AD1457',
                  fontSize: 18,
                  fontFamily: 'Montserrat-Regular',
                  fontWeight: 'bold',
                }}>
                No Invitations
              </Text>
            </View>
          ) : null}
          {this.state.isLoading ? (
            <Loader text={'Loading Data'} />
          ) : (
            <List style={styles.list}>
              {this.state.invites.map(invite => {
                return (
                  <ListItem
                    key={invite.NoteKey}
                    style={{
                      marginLeft: 0,
                      paddingLeft: 20,
                      marginRight: 0,
                      paddingRight: 0,
                    }}>
                    <Body>
                      <View
                        style={{
                          flexDirection: 'column',
                          justifyContent: 'space-around',
                        }}>
                        <Text style={{fontSize: 18}}>{invite.Subject}</Text>
                        <Text note>{invite.Description}</Text>
                      </View>
                    </Body>
                    <Right>
                      <View style={styles.touchableContainer}>
                        <TouchableOpacity
                          style={styles.touchable}
                          onPress={() => {
                            this.props.navigation.navigate('Confirm', invite);
                          }}>
                          <Icon
                            name="md-eye"
                            style={[styles.iconStyle, {color: '#AD1457'}]}
                          />
                        </TouchableOpacity>
                      </View>
                    </Right>
                  </ListItem>
                );
              })}
            </List>
          )}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  touchableContainer: {
    width: '100%',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    alignContent: 'flex-start',
    flexDirection: 'row',
  },
  iconStyle: {
    color: '#2196F3',
    fontSize: 28,
  },
  list: {
    flex: 1,
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  },
  addButton: {
    width: '96%',
    marginBottom: 10,
    alignSelf: 'center',
    backgroundColor: '#2196F3',
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});

function mapStateToProps(state) {
  return {
    currUser: state.currentUser,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      currentUser,
    },
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Invites);
