import React from 'react';
import {createSwitchNavigator} from 'react-navigation';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import NoteContainer from './Notes/Index';
import AuthContainer from './Login/Index';
import Loadingscreen from './Loading/LoadingScreen';
import ProfileTab from './Profile/Profile';
import Invitations from './Invitations';
import Users from './Users/Users';

import {View} from 'react-native';
import {Icon} from 'native-base';

const AppTabNavigator = createBottomTabNavigator(
  {
    Note: {
      screen: NoteContainer,
      navigationOptions: {
        title: 'Notes',
        tabBarIcon: ({tintColor}) => (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
              name="md-paper"
              style={{fontSize: 20, color: tintColor, marginTop: 8}}
            />
          </View>
        ),
      },
    },
    Invitations: {
      screen: Invitations,
      navigationOptions: {
        title: 'Invitations',
        tabBarIcon: ({tintColor}) => (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
              name="md-mail"
              style={{fontSize: 25, color: tintColor, marginTop: 8}}
            />
          </View>
        ),
      },
    },
    Users: {
      screen: Users,
      navigationOptions: {
        title: 'Users',
        tabBarIcon: ({tintColor}) => (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
              name="md-people"
              style={{fontSize: 30, color: tintColor, marginTop: 8}}
            />
          </View>
        ),
      },
    },
    ProfileTab: {
      screen: ProfileTab,
      navigationOptions: {
        title: 'Profile',
        tabBarIcon: ({tintColor}) => (
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
              name="md-person"
              style={{fontSize: 25, color: tintColor, marginTop: 8}}
            />
          </View>
        ),
      },
    },
  },
  {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      style: {
        ...Platform.select({
          android: {
            backgroundColor: 'white',
          },
        }),
      },
      activeTintColor: '#AD1457',
      inactiveTintColor: '#9E9E9E',
      showLabel: true,
      showIcon: true,
    },
  },
);

const AppContainer = createAppContainer(AppTabNavigator);

const SwitchStack = createSwitchNavigator(
  {
    Loadingscreen,
    Auth: AuthContainer,
    App: AppContainer,
  },
  {
    initialRouteName: 'Loadingscreen',
  },
);

const SwitchContainer = createAppContainer(SwitchStack);

export default SwitchContainer;
