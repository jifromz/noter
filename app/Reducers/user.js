 
import * as actionTypes from "../Actions/Users/usersActionTypes";

const initialState = {
  users: {},
  isFetching: true
};

export default user = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER:
      return {
        ...state,
        isFetching: true
      };
    case actionTypes.FETCH_USER_FULFILLED:
      return {
        ...state,
        isFetching: false,
        user:  action.payload
      };
    case actionTypes.FETCH_USER_REJECTED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      break;
  }
  return state;
};


