 
import * as actionTypes from "../Actions/Notes/notesActionTypes";

const initialState = {
  notes: [],
  isFetching: true
};

export default notes = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_NOTES:
      return {
        ...state,
        isFetching: true
      };
    case actionTypes.FETCH_NOTES_FULFILLED:
      return {
        ...state,
        isFetching: false,
        notes:  action.payload
      };
    case actionTypes.FETCH_NOTES_REJECTED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      break;
  }
  return state;
};


