import * as actionTypes from '../Actions/Users/usersActionTypes';

const initialState = {
  createUser: {
    isSuccess: false,
    message: '',
    isSaving: false,
    code: '',
    user: null,
    error: false,
  },
};

export default createUser = (state = initialState, action) => {
  let result = {};

  switch (action.type) {
    case actionTypes.SIGNUP_USER:
      result.isSuccess = false;
      result.isSaving = true;
      result.message = '';
      result.code = '';
      result.user = null;
      state.createUser.isSaving = true;
      return {
        ...state,
        createUser: result,
      };
    case actionTypes.SIGNUP_USER_FULFILLED:
      if (action.payload.code) {      
        result.isSuccess = true;
        result.isSaving = false;
        result.message = action.payload.message;
        result.code = action.payload.code;
        result.user = null;       
      }
      if (action.payload.user) {
        result.isSuccess = true;
        result.isSaving = false;
        result.message = '';
        result.code = '';
        result.user = action.payload.user;
      }
      return {
        ...state,
        createUser: result,
      };
    case actionTypes.SIGNUP_USER_REJECTED:
      result.error = true;
      return {
        ...state,
        createUser: result,
      };
    default:
      return state;
  }
};
