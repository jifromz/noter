import * as actionTypes from "../Actions/Users/usersActionTypes";

const initialState = {
  currentUser: {
      email: '',
      uid: '',
      displayName: '',
      refreshToken: ''
  }
};

export default currentUser = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload
      };
     
    default:
      break;
  }
  return state;
};


