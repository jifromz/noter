import { combineReducers } from "redux";

import notes from "./notes";
import note from './note';
import users from "./users";
import user from "./user";
import currentUser from './currentUser';


const appReducer = combineReducers({
  // users,
  // note,
  // notes,
  // user,
  currentUser
});

export default appReducer;
