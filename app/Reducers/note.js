 
import * as actionTypes from "../Actions/Notes/notesActionTypes";

const initialState = {
  note: {},
  isFetching: true
};

export default note = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_NOTE:
      return {
        ...state,
        isFetching: true
      };
    case actionTypes.FETCH_NOTE_FULFILLED:
      return {
        ...state,
        isFetching: false,
        note:  action.payload
      };
    case actionTypes.FETCH_NOTE_REJECTED:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    default:
      break;
  }
  return state;
};


