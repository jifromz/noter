import firebaseX from '../FirebaseConfig';

export const getNotes = async () => {
  let q = noteId ? 'Notes/' + noteId : 'Notes/';
  await firebase
    .database()
    .ref(q)
    .once('value', function(res) {
      return res.val();
    });
};

export const deleteNote = async noteId => {
  let q = 'Notes/' + noteId;
  await firebase
    .database()
    .ref(q)
    .remove('value', function(res) {
      return res.val();
    });
};
