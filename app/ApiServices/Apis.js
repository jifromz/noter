import firebaseX from '../FirebaseConfig';

//--------------USERS-------------------

export const readUsersData = async () => {
  return await firebaseX
    .database()
    .ref('Users/')
    .once('value')
    .then(notes => {
      return notes.val();
    });
};

export const writeUserData = param => {
  return firebaseX
    .database()
    .ref('Users/')
    .push({
      Email: param.email,
      Password: param.password,
      Name: param.name,
      Device_Token: param.deviceToken,
    })
    .then(data => {
      return data.toJSON();
    })
    .catch(error => {
      return error;
    });
};

export const createUser = async creds => {
  return await firebaseX
    .auth()
    .createUserWithEmailAndPassword(creds.email, creds.password)
    .then(res => {
      return res;
    })
    .catch(err => {
      return err;
    });
};

export const deleteUser = key => {
  let q = 'Users/' + key;
  return firebaseX
    .database()
    .ref(q)
    .remove();
};

//--------------NOTES-------------------

export const readNotesData = () => {
  return firebaseX
    .database()
    .ref('Notes/')
    .once('value')
    .then(notes => {
      return notes.val();
    })
    .catch(() => {
      return [];
    });
};

export const readNotesDataByNoteId = async noteId => {
  let q = 'Notes/' + noteId;
  return await firebaseX
    .database()
    .ref(q)
    .once('value')
    .then(note => {
      return note.val();
    })
    .catch(() => {
      return null;
    });
};

export const writeNoteData = param => {
  return firebaseX
    .database()
    .ref('Notes/')
    .push({
      Created_By: param.Created_By,
      Date_End: param.Date_End,
      Date_Start: param.Date_Start,
      Description: param.Description,
      Email: param.Email,
      Subject: param.Subject,
    })
    .then(() => {
      return true;
    })
    .catch(() => {
      return false;
    });
};

export const deleteNote = key => {
  let q = 'Notes/' + key;
  return firebaseX
    .database()
    .ref(q)
    .remove();
};

export const updateNoteData = param => {
  return firebaseX
    .database()
    .ref('Notes/' + param.Key)
    .update(
      {
        Created_By: param.Created_By,
        Date_End: param.Date_End,
        Date_Start: param.Date_Start,
        Description: param.Description,
        Subject: param.Subject,
      },
      error => {
        if (error) {
          return error;
        } else {
          return true;
        }
      },
    );
};

//---------ATTENDANCE--------------------------
export const updateAttendance = param => {
  console.log(param.data)
  return firebaseX
    .database()
    .ref('Attendance/' + param.Key + '/Users/' + param.userKey)
    .update(param.data, error => {
      if (error) {
        return error;
      } else {
        return true;
      }
    });
};

export const writeAttendanceData = param => {
  return firebaseX
    .database()
    .ref('Attendance/')
    .child(param.id)
    .set(param.obj)
    .then(() => {
      return true;
    })
    .catch(() => {
      return false;
    });
};

export const readAttendanceData = async key => {
  return await firebaseX
    .database()
    .ref('Attendance/' + key)
    .once('value')
    .then(attendance => {
      return attendance.val();
    });
};

export const readAllAttendanceData = async () => {
  return await firebaseX
    .database()
    .ref(`Attendance/`)
    .once('value')
    .then(attendance => {
      return attendance.val();
    });
};
