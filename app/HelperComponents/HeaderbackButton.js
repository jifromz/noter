import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Icon} from 'native-base';

const BackButton = ({onPress}) => (
  <View style={{marginLeft: 15, flex: 1, justifyContent:'center', alignItems: 'center', width: '100%'}}>
    <TouchableOpacity
      onPress={onPress}>
      <Icon name="arrow-back" style={{fontSize: 24, color: '#fff'}} />
    </TouchableOpacity>
  </View>
);

export default BackButton;
