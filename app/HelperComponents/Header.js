import React, { Component } from "react";
import { Text, View } from "native-base";
 

export default (header = ({ title }) => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContente: "center" }}>
      <Text
        style={{ fontSize: 20,   color: "#000000" }}
      >
        {title}
      </Text>
    </View>
    
  );
});
