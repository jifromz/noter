import React, {Component} from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';

const Loader = ({text}) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color="#AD1657" />
      <Text
        style={{fontSize: 16, color: '#AD1657', fontFamily: 'Montserrat-Bold'}}>
        {text}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
});

export default Loader;
