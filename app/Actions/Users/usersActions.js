import * as actionTypes from "./usersActionTypes";

 

import {createUser} from '../../ApiServices/Apis';
 


export function currentUser(payload){
  return {
    type: actionTypes.CURRENT_USER,
    payload: payload
  }
}

export function userSignUp(payload){
    return {
        type: actionTypes.SIGNUP_USER,
        payload: createUser(payload)
    }
}



//old
export function fetchUsers() {
  return {
    type: actionTypes.FETCH_USERS,
    payload: getUsers(null)
  }
}

export function fetchUser(payload) {
  return {
    type: actionTypes.FETCH_USER,
    payload: getUsers(payload)
  }
}
