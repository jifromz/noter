import * as actionTypes from './notesActionTypes';

import {getNotes, deleteNote} from '../../ApiServices/notesApi';

export function fetchNotes() {
  return {
    type: actionTypes.FETCH_NOTES,
    payload: getNotes(null),
  };
}

export function fetchNote(payload) {
  return {
    type: actionTypes.FETCH_NOTES,
    payload: getNotes(payload),
  };
}

export function deleteNote(payload) {
  return {
    type: actionTypes.FETCH_NOTES,
    payload: deleteNote(payload),
  };
}
